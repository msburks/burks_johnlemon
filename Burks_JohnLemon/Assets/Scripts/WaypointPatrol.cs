﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    //establishes all necessary variables
    public NavMeshAgent navMeshAgent;
    public Transform[] waypoints;
    int m_CurrentWaypointIndex;

    // sets waypoint array for enemies to navigate between
    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    
    void Update()
    {
        // checks distance between waypoints for enemies
        if (navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance)
        {
            //iterates between set waypoints until there are non remaining
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
        }
    }
}
