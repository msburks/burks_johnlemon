﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlayerMovement : MonoBehaviour
{
    //establishes all necessary variables 
    Vector3 m_Movement;
    Animator m_Animator;
    
    public float turnSpeed = 20f;
    public float freezeTime;
    public float shrinkTime;
    public float growTime;
    Quaternion m_Rotation = Quaternion.identity;
    Rigidbody m_Rigidbody;
    //all the doors to be unlocked for victory to be acieved
    public GameObject door;
    public GameObject door1;
    public GameObject door2;
    public GameObject door3;
    public GameObject door4;

    public AudioSource m_AudioSource;
    public AudioSource pickupSound;
    

    WaypointPatrol[] allPatrols;//array of all waypoints
    public GameObject[] allEnemies;//array of all enemies

    //public string mat_unactivated = "Mat_Button";
    public Material mat_activated;//allows reference to specific material
    public Renderer mat_renderer;//reference to specific renderer, all following mat_renders are for specific buttons
    public Renderer mat_renderer1;
    public Renderer mat_renderer2;
    public Renderer mat_renderer3;
    public Renderer mat_renderer4;
    public int buttons_left = 5;//number of buttons to be activated for game vitory
    public TextMeshProUGUI buttons_text;//canvas for buttons counter

    void Start()
    {
        //gets all necessary components from player character object
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        //m_AudioSource = GetComponent<AudioSource>();

        allPatrols = FindObjectsOfType<WaypointPatrol>();
        
    }

    void Update()
    {
        buttons_text.text = "Buttons Remaining: " + buttons_left;
    }
    //function initialing the freezing of enemies
    void FreezeGhost()
    {
        foreach (WaypointPatrol patrol in allPatrols){//referencing each waypoint in the array
            if (patrol != null)
            {
                patrol.navMeshAgent.isStopped = true;//stops the waypoint for a period of time
            }
        }
        CancelInvoke("EndFreeze");
        Invoke("EndFreeze", freezeTime);
    }
    //Function ending the enemy waypoint freeze
    void EndFreeze()
    {
        foreach (WaypointPatrol patrol in allPatrols)
        {
            if (patrol != null)
            {
                patrol.navMeshAgent.isStopped = false;//returns the waypoints to being carries out
            }
        }

    }
    //function to shrink enemies
    void Shrink()
    {
        foreach (GameObject enemy in allEnemies)//iterates through allEnemies array
        {
            if (enemy != null)
            {
                enemy.transform.localScale = new Vector3(.5f, .5f, .5f);//sets new local scale for each enemy
            }
        }
        CancelInvoke("Growback");//these two lines invoke the enemies to growback after a period of time
        Invoke("Growback", shrinkTime);
    }
    //fucntion to counteract the shrinking function
    void Growback()
    {
        foreach (GameObject enemy in allEnemies)
        {
            if (enemy != null)
            {
                enemy.transform.localScale = new Vector3(1f, 1f, 1f);//returns scale to original values
            }
        }
    }
    //makes enemies double in size
    void GrowBig()
    {
        foreach (GameObject enemy in allEnemies)
        {
            if (enemy != null)
            {
                enemy.transform.localScale = new Vector3(2f, 2f, 2f);//returns scale to original values
            }
        }
        CancelInvoke("GrowSmall");//these two lines invoke the enemies to GrowSmall after a period of time
        Invoke("GrowSmall", growTime);
    }
    //function to counteract the GrowBig function
    void GrowSmall()
    {
        foreach (GameObject enemy in allEnemies)
        {
            if (enemy != null)
            {
                enemy.transform.localScale = new Vector3(1f, 1f, 1f);//returns scale to original values
            }
        }
    }



    //functions to disable the door objects, accounting for each individual one
    void OpenDoor()
    {
        door.SetActive(false);
    }

    void OpenDoor1()
    {
        door1.SetActive(false);
    }

    void OpenDoor2()
    {
        door2.SetActive(false);
    }

    void OpenDoor3()
    {
        door3.SetActive(false);
    }

    void OpenDoor4()
    {
        door4.SetActive(false);
    }

    // decreases button counter
    void Decrease_button()
    {
        buttons_left -= 1;
    }
    //Trigger collider functions
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("PickUp1")) //compares tag to see if matches str
        {
            other.gameObject.SetActive(false);
            pickupSound.Play();
            FreezeGhost();
        }
        if (other.gameObject.CompareTag("PickUp2"))//compares tag to see if matches str
        {
            other.gameObject.SetActive(false);
            pickupSound.Play();
            Shrink();
        }
        if (other.gameObject.CompareTag("PickUpGrow"))//compares tag to see if matches str
        {
            other.gameObject.SetActive(false);
            pickupSound.Play();
            GrowBig();
        }

        //all button trigger if statements coordinate between specific buttons and specific doors
        if (other.gameObject.CompareTag("Button"))//compares tag to see if matches str
        {
            //other.gameObject.transform.position = new Vector3 (0f, -0.95f, 3);
            
            mat_renderer.material = mat_activated;
            pickupSound.Play();
            OpenDoor();
            Decrease_button();


        }
        if (other.gameObject.CompareTag("Button1"))//compares tag to see if matches str
        {
            //other.gameObject.transform.position = new Vector3 (0f, -0.95f, 3);
            
            mat_renderer1.material = mat_activated;
            pickupSound.Play();
            OpenDoor1();
            Decrease_button();


        }
        if (other.gameObject.CompareTag("Button2"))//compares tag to see if matches str
        {
            //other.gameObject.transform.position = new Vector3 (0f, -0.95f, 3);
            
            mat_renderer2.material = mat_activated;
            pickupSound.Play();
            OpenDoor2();
            Decrease_button();


        }
        if (other.gameObject.CompareTag("Button3"))//compares tag to see if matches str
        {
            //other.gameObject.transform.position = new Vector3 (0f, -0.95f, 3);
            
            mat_renderer3.material = mat_activated;
            pickupSound.Play();
            OpenDoor3();
            Decrease_button();


        }
        if (other.gameObject.CompareTag("Button4"))//compares tag to see if matches str
        {
            //other.gameObject.transform.position = new Vector3 (0f, -0.95f, 3);
            
            mat_renderer4.material = mat_activated;
            pickupSound.Play();
            OpenDoor4();
            Decrease_button();


        }
    }
     
    void FixedUpdate()
    {
        
        //establishes player axises and inputs
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");
        //updated movement using axises
        m_Movement.Set(horizontal, 0f, vertical);
        m_Movement.Normalize();

        //calculates movement
        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        bool hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        bool isWalking = hasHorizontalInput || hasVerticalInput;
        //is walking threshold to toggle between idle and walking animation
        m_Animator.SetBool("IsWalking", isWalking);

        //audio source for footsteps (start or stop)
        if (isWalking)
        {
            if (!m_AudioSource.isPlaying)
            {
                m_AudioSource.Play();
            }
        }
        else
        {
            m_AudioSource.Stop();
        }

        //creates movement and defines time perameters 
        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        m_Rotation = Quaternion.LookRotation(desiredForward);
    }

    //allows root motion to be applied with animation of player character
    void OnAnimatorMove()
    {
        m_Rigidbody.MovePosition(m_Rigidbody.position + m_Movement * m_Animator.deltaPosition.magnitude);
        m_Rigidbody.MoveRotation(m_Rotation);
    }

}
