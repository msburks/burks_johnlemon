﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameEnding : MonoBehaviour
{
    //Assigning all necessary variables

    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    //variables for ending/cuaght images and audio
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public float timeLeft;//timer variable
    public TextMeshProUGUI timer_text;//canvas for timer

    bool m_IsPlayerCaught;
    float m_Timer;
    bool m_HasAudioPlayed;
    bool m_IsPlayerAtExit;

    //sets trigger between player object and collider, changes isplayeratexit bool
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            m_IsPlayerAtExit = true;
        }
    }
    //function helping determine if player is caught by enemy
    public void CaughtPlayer()
    {
        m_IsPlayerCaught = true;
    }

    //end game if the if statement condition is met
    void Update()
    { 

        //checks for if player is at exit trigger, displays image, plays audio
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);

        }
        else if (m_IsPlayerCaught)
        {
            //checks if caught by enemy, displays image, plays audio
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        //sets timer up, amount of time is public variable
        timeLeft -= Time.deltaTime;
        if (timeLeft <= 0f)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
        timer_text.text = "Time Remaining: " + (int)timeLeft;//sets timer into the TMPro canvas
    }

    //function to initiate the game ending, parameters involve image, restart boolean, and audio source
    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        //plays audio once triggered
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        // timer for fade in of image over scene
        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        //lets the game know what to do after the image display duration and fade duration is exceeded by the timer
        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(1);//reloads original sccene if caught
            }
            else
            {
                Application.Quit();
            }
        }
    }
}